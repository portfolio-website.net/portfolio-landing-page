---
publishDate: 2024-01-12T00:00:00Z
title: '4 Billion If Statements'
excerpt: This post shows how to create a functioning program with 4 billion if statements.
image: ~/assets/images/pexels-christina-morillo-1181271.jpg
tags:
  - development
  - articles
---

<a href="https://www.pexels.com/photo/black-and-gray-laptop-computer-turned-on-doing-computer-codes-1181271/" rel="noopener noreferrer" class="photo-credit">Photo by Christina Morillo from Pexels</a>

<a href="https://andreasjhkarlsson.github.io/jekyll/update/2023/12/27/4-billion-if-statements.html" rel="noopener noreferrer">4 Billion If Statements</a> shows how to determine if a number is even or odd using 4 billion `if` statements.  Also, see the <a href="https://news.ycombinator.com/item?id=38790597" rel="noopener noreferrer">Hacker News</a> comments.