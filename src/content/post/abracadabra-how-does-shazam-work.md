---
publishDate: 2024-01-22T00:00:00Z
title: 'Abracadabra: How Does Shazam Work? (2022)'
excerpt: Discover details about identifying any song via a mobile device.
image: ~/assets/images/pexels-george-webster-213207.jpg
tags:
  - articles
---

<a href="https://www.pexels.com/photo/people-on-concert-213207/" rel="noopener noreferrer" class="photo-credit">Photo by George Webster from Pexels</a>

<a href="https://www.cameronmacleod.com/blog/how-does-shazam-work" rel="noopener noreferrer">Abracadabra: How Does Shazam Work?</a> explains the technical concepts needed to identify any song.  Also, see the <a href="https://news.ycombinator.com/item?id=38531428" rel="noopener noreferrer">Hacker News</a> comments.