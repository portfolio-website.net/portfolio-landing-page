---
publishDate: 2024-01-20T00:00:00Z
title: 'Adventures with Compression'
excerpt: Learn about some rabbit holes encountered when attempting to compress a large file.
image: ~/assets/images/pexels-pixabay-159866.jpg
tags:
  - development
  - articles
---

<a href="https://www.pexels.com/photo/pile-of-books-159866/" rel="noopener noreferrer" class="photo-credit">Photo by Pixabay from Pexels</a>

<a href="https://jamesg.blog/2023/12/29/compression-adventures/" rel="noopener noreferrer">Adventures with Compression</a> shares some knowledge on how to compress a large file -- 1 GB of Wikipedia data -- as part of a competition.  Also, see the <a href="https://news.ycombinator.com/item?id=38813700" rel="noopener noreferrer">Hacker News</a> comments.