---
publishDate: 2024-01-19T00:00:00Z
title: 'AI Explained'
excerpt: This YouTube channel covers topics related to AI.
image: ~/assets/images/pexels-kindel-media-9028884.jpg
tags:
  - ai
  - videos
---

<a href="https://www.pexels.com/photo/white-and-gray-robot-toy-9028884/" rel="noopener noreferrer" class="photo-credit">Photo by Kindel Media from Pexels</a>

<a href="https://www.youtube.com/@aiexplained-official" rel="noopener noreferrer">AI Explained</a> is a YouTube channel covering topics related to AI.