---
publishDate: 2024-01-29T00:00:00Z
title: 'All Code Is Technical Debt'
excerpt: Learn about the concept of delivering value to users, and not just adding more code.
image: ~/assets/images/pexels-thisisengineering-3861972.jpg
tags:
  - articles
---

<a href="https://www.pexels.com/photo/female-software-engineer-coding-on-computer-3861972/" rel="noopener noreferrer" class="photo-credit">Photo by ThisIsEngineering from Pexels</a>

<a href="https://www.tokyodev.com/articles/all-code-is-technical-debt" rel="noopener noreferrer">All Code Is Technical Debt</a> has an interesting take on how developers should focus on producing quality products, rather than simply churning out code.  Also, see the <a href="https://news.ycombinator.com/item?id=38694051" rel="noopener noreferrer">Hacker News</a> comments.