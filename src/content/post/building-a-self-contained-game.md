---
publishDate: 2024-02-02T00:00:00Z
title: 'Building a Self-Contained Game in C# under 2 Kilobytes'
excerpt: Learn how to minimize the size of a graphical maze game.
image: ~/assets/images/pexels-hüseyin-fatih-özden-12040985.jpg
tags:
  - development
  - articles
---

<a href="https://www.pexels.com/photo/maze-out-of-hedge-12040985/" rel="noopener noreferrer" class="photo-credit">Photo by Hüseyin Fatih ÖZDEN from Pexels</a>

<a href="https://migeel.sk/blog/2024/01/02/building-a-self-contained-game-in-csharp-under-2-kilobytes/" rel="noopener noreferrer">Building a Self-Contained Game in C# under 2 Kilobytes</a> shows how to shrink a C# graphical maze game into 2 kB.  Also, see the <a href="https://news.ycombinator.com/item?id=38845059" rel="noopener noreferrer">Hacker News</a> comments.