---
publishDate: 2024-02-03T00:00:00Z
title: 'Can You Fit a Whole Game into a QR Code?'
excerpt: See how to create a snake game encoded in a QR code.
image: ~/assets/images/pexels-pixabay-278430.jpg
tags:
  - development
  - articles
---

<a href="https://www.pexels.com/photo/qr-code-on-screengrab-278430/" rel="noopener noreferrer" class="photo-credit">Photo by Pixabay from Pexels</a>

MattKC shows how to build a snake game and fit it into a QR code.

<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="//www.youtube-nocookie.com/embed/ExwqNreocpg?rel=0&amp;showinfo=0" width="300" height="150" allowfullscreen="allowfullscreen" data-mce-fragment="1"></iframe></div>

Try the game: <a href="https://www.mattkc.com/etc/snakeqr/" rel="noopener noreferrer">Snake in a QR code</a>