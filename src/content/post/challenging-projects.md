---
publishDate: 2024-01-10T00:00:00Z
title: 'Challenging Projects Every Programmer Should Try (2019)'
excerpt: Check out some suggested projects ideas to build as a software developer.
image: ~/assets/images/pexels-fauxels-3183127.jpg
tags:
  - development
  - articles
---

<a href="https://www.pexels.com/photo/photo-of-people-near-laptops-3183127/" rel="noopener noreferrer" class="photo-credit">Photo by fauxels from Pexels</a>

<a href="https://austinhenley.com/blog/challengingprojects.html" rel="noopener noreferrer">Challenging Projects Every Programmer Should Try</a> describes some good projects software developers can create to learn something new.  Also, see the <a href="https://news.ycombinator.com/item?id=21790779" rel="noopener noreferrer">Hacker News</a> (<a href="https://news.ycombinator.com/item?id=38768678" rel="noopener noreferrer">reposted for 2023</a>) and <a href="https://www.reddit.com/r/programming/comments/ean6ch/challenging_projects_every_programmer_should_try/" rel="noopener noreferrer">Reddit</a> comments for some intriguing discussion.