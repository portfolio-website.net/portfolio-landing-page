---
publishDate: 2024-01-30T00:00:00Z
title: 'Cold-Blooded Software'
excerpt: Read about the concept of boring software that shouldn't require ongoing maintenance to continue working.
image: ~/assets/images/pexels-magda-ehlers-3753182.jpg
tags:
  - articles
---

<a href="https://www.pexels.com/photo/photo-of-small-turtle-on-soil-3753182/" rel="noopener noreferrer" class="photo-credit">Photo by Magda Ehlers from Pexels</a>

<a href="https://dubroy.com/blog/cold-blooded-software/" rel="noopener noreferrer">Cold-Blooded Software</a> thoughtfully describes software that uses boring technology that continues to work without modification.  Also, see the <a href="https://news.ycombinator.com/item?id=38793206" rel="noopener noreferrer">Hacker News</a> comments.