---
publishDate: 2024-01-16T00:00:00Z
title: 'Committing without Git'
excerpt: This fun post shows how to create a commit without using Git.
image: ~/assets/images/pexels-realtoughcandycom-11035539.jpg
tags:
  - development
  - articles
---

<a href="https://www.pexels.com/photo/person-holding-a-small-paper-11035539/" rel="noopener noreferrer" class="photo-credit">Photo by RealToughCandy.com from Pexels</a>

<a href="https://matheustavares.gitlab.io/posts/committing-without-git" rel="noopener noreferrer">Committing without Git</a> demonstrates how to manually construct a commit without actually using the `git` command.  Also, see the <a href="https://news.ycombinator.com/item?id=38814492" rel="noopener noreferrer">Hacker News</a> comments.