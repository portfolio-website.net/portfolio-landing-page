---
publishDate: 2024-01-18T00:00:00Z
title: 'Email Addresses Are Not Good ‘Permanent’ Identifiers for Accounts'
excerpt: This post discusses common challenges when a system needs to identify individual users.
image: ~/assets/images/pexels-torsten-dettlaff-193003.jpg
tags:
  - development
  - articles
---

<a href="https://www.pexels.com/photo/black-and-gray-digital-device-193003/" rel="noopener noreferrer" class="photo-credit">Photo by Torsten Dettlaff from Pexels</a>

<a href="https://utcc.utoronto.ca/~cks/space/blog/tech/EmailAddressesBadPermanentIDs" rel="noopener noreferrer">Email Addresses Are Not Good ‘Permanent’ Identifiers for Accounts</a> addresses some of the common challenges encountered when attempting to uniquely identify users.  Also, see the <a href="https://news.ycombinator.com/item?id=38823817" rel="noopener noreferrer">Hacker News</a> comments.