---
publishDate: 2024-01-26T00:00:00Z
title: 'Git Branches: Intuition & Reality'
excerpt: Learn how Git works with branches internally.
image: ~/assets/images/pexels-christina-morillo-1181253.jpg
tags:
  - development
  - articles
---

<a href="https://www.pexels.com/photo/eyeglasses-in-front-of-laptop-computer-1181253/" rel="noopener noreferrer" class="photo-credit">Photo by Christina Morillo from Pexels</a>

<a href="https://jvns.ca/blog/2023/11/23/branches-intuition-reality/" rel="noopener noreferrer">Git Branches: Intuition & Reality</a> discusses some internal details about how Git branches work.  Also, see the <a href="https://news.ycombinator.com/item?id=38393238" rel="noopener noreferrer">Hacker News</a> comments.