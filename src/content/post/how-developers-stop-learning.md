---
publishDate: 2024-01-06T00:00:00Z
title: 'How Developers Stop Learning: Rise of the Expert Beginner (2012)'
excerpt: Learn about a phenomenon potentially impacting developer expertise in the workplace.
image: ~/assets/images/pexels-oluwaseun-duncan-226232.jpg
tags:
  - development
  - articles
---

<a href="https://www.pexels.com/photo/macbook-air-turned-on-226232/" rel="noopener noreferrer" class="photo-credit">Photo by Oluwaseun Duncan from Pexels</a>

A series beginning with <a href="https://daedtech.com/how-developers-stop-learning-rise-of-the-expert-beginner/" rel="noopener noreferrer">How Developers Stop Learning: Rise of the Expert Beginner</a> details a dilemma developers may encounter as they gain professional experience.  The article includes a reference to the popular quote, <a href="https://news.ycombinator.com/item?id=4627373" rel="noopener noreferrer">"Some people have 10 years of experience. Others have 1 year of experience 10 times."</a> to help explain how some individuals become stuck as expert beginners during their career.
