---
publishDate: 2024-01-23T00:00:00Z
title: 'How Google Takes the Pain out of Code Reviews, with 97% Dev Satisfaction'
excerpt: Learn about Google's code review tooling (Critique).
image: ~/assets/images/pexels-danny-meneses-943096.jpg
tags:
  - development
  - articles
---

<a href="https://www.pexels.com/photo/photo-of-turned-on-laptop-computer-943096/" rel="noopener noreferrer" class="photo-credit">Photo by Danny Meneses from Pexels</a>

<a href="https://read.engineerscodex.com/p/how-google-takes-the-pain-out-of" rel="noopener noreferrer">How Google Takes the Pain out of Code Reviews, with 97% Dev Satisfaction</a> discusses Google's code review guidelines and how their tool, Critique, allows developers to efficiently review and submit changes.  Also, see the <a href="https://news.ycombinator.com/item?id=38518473" rel="noopener noreferrer">Hacker News</a> comments.