---
publishDate: 2024-01-13T00:00:00Z
title: 'Is Software Getting Worse?'
excerpt: Are customers paying the price from unoptimized software programs?
image: ~/assets/images/pexels-deyvi-romero-89955.jpg
tags:
  - development
  - articles
---

<a href="https://www.pexels.com/photo/black-android-smartphone-showing-instagram-and-gmail-application-89955/" rel="noopener noreferrer" class="photo-credit">Photo by Deyvi Romero from Pexels</a>

<a href="https://stackoverflow.blog/2023/12/25/is-software-getting-worse/" rel="noopener noreferrer">Is Software Getting Worse?</a> poses some fascinating questions about the current state of perceived waste and inefficiency of software programs.  Also, see the <a href="https://news.ycombinator.com/item?id=38764427" rel="noopener noreferrer">Hacker News</a> comments.