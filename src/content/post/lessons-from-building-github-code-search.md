---
publishDate: 2024-01-21T00:00:00Z
title: '“Lessons from Building GitHub Code Search” by Luke Francl (Strange Loop 2023)'
excerpt: Learn about the new GitHub search engine, Blackbird.
image: ~/assets/images/pexels-evg-kowalievska-1174775.jpg
tags:
  - articles
---

<a href="https://www.pexels.com/photo/person-typing-on-laptop-1174775/" rel="noopener noreferrer" class="photo-credit">Photo by EVG Kowalievska from Pexels</a>

Luke Francl presents on the lessons learned building the new GitHub search engine, Blackbird.

<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="//www.youtube-nocookie.com/embed/CqZA_KmygKw?rel=0&amp;showinfo=0" width="300" height="150" allowfullscreen="allowfullscreen" data-mce-fragment="1"></iframe></div>

See the <a href="https://news.ycombinator.com/item?id=38637652" rel="noopener noreferrer">Hacker News</a> comments for additional discussion.