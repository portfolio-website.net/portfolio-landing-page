---
publishDate: 2024-01-01T00:00:00Z
title: '‘Merchants of Complexity’: Why 37Signals Abandoned the Cloud'
excerpt: Does cloud hosting make sense for every company, or does it add extra complexity?  Learn how it's possible to save money moving off the cloud.
image: ~/assets/images/pexels-aleksandar-pasaric-325185.jpg
tags:
  - cloud-services
  - articles
---

<a href="https://www.pexels.com/photo/view-of-cityscape-325185/" rel="noopener noreferrer" class="photo-credit">Photo by Aleksandar Pasaric from Pexels</a>

The article <a href="https://thenewstack.io/merchants-of-complexity-why-37signals-abandoned-the-cloud/" rel="noopener noreferrer">‘Merchants of Complexity’: Why 37Signals Abandoned the Cloud</a> provides some insight into the value proposition of deploying to the cloud vs. using on-prem servers.  Also, see the <a href="https://news.ycombinator.com/item?id=38778388" rel="noopener noreferrer">Hacker News comments</a> for some intriguing discussion.
