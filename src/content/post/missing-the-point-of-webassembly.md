---
publishDate: 2024-02-08T00:00:00Z
title: 'Missing the Point of WebAssembly'
excerpt: Read about how Wasm may be operationally described as a new fundamental abstraction boundary.
image: ~/assets/images/pexels-thisisengineering-3862132.jpg
tags:
  - articles
---

<a href="https://www.pexels.com/photo/female-engineer-controlling-flight-simulator-3862132/" rel="noopener noreferrer" class="photo-credit">Photo by ThisIsEngineering from Pexels</a>

<a href="https://wingolog.org/archives/2024/01/08/missing-the-point-of-webassembly" rel="noopener noreferrer">Missing the Point of WebAssembly</a> contemplates how Wasm can be effectively described as going beyond a virtual machine and help to break a system into composable components with a software-defined abstraction boundary.  Also, see the <a href="https://news.ycombinator.com/item?id=38927960" rel="noopener noreferrer">Hacker News</a> comments.