---
publishDate: 2024-01-24T00:00:00Z
title: 'Not a Real Engineer (2019)'
excerpt: This thoughtful poem considers what it really means to be an engineer.
image: ~/assets/images/pexels-ruslan-burlaka-140945.jpg
tags:
  - articles
---

<a href="https://www.pexels.com/photo/black-and-gray-photo-of-person-in-front-of-computer-monitor-140945/" rel="noopener noreferrer" class="photo-credit">Photo by Ruslan Burlaka from Pexels</a>

<a href="https://twitchard.github.io/posts/2019-05-29-not-a-real-engineer.html" rel="noopener noreferrer">Not a Real Engineer</a> is a poem about rejection from a technical engineering position.  Also, see the <a href="https://news.ycombinator.com/item?id=38503486" rel="noopener noreferrer">Hacker News</a> comments.