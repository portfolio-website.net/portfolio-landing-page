---
publishDate: 2024-01-04T00:00:00Z
title: 'Now Add a Walrus: Prompt Engineering in DALL-E 3 (2023)'
excerpt: This experiment shows how to provide rendering feedback for DALL-E 3 images using a prior generation seed (plus a look at the leaked prompt).
image: ~/assets/images/pexels-rutpratheep-nilpechr-11092336.jpg
tags:
  - ai
  - articles
---

<a href="https://www.pexels.com/photo/close-up-of-walrus-11092336/" rel="noopener noreferrer" class="photo-credit">Photo by Rutpratheep Nilpechr from Pexels</a>

<a href="https://simonwillison.net/2023/Oct/26/add-a-walrus/" rel="noopener noreferrer">Now Add a Walrus: Prompt Engineering in DALL-E 3</a> is an experimental post that shows how to work with ChatGPT Plus and DALL-E 3.
