---
publishDate: 2024-01-03T00:00:00Z
title: 'Personal Blog Links (2023)'
excerpt: Explore a list of personal blogs shared by the Hacker News community.
image: ~/assets/images/pexels-picography-4458.jpg
tags:
  - links
---

<a href="https://www.pexels.com/photo/white-ceramic-mug-between-apple-magic-keyboard-and-two-flat-screen-computer-monitors-4458/" rel="noopener noreferrer" class="photo-credit">Photo by Picography from Pexels</a>

Originally posted at <a href="https://news.ycombinator.com/item?id=36575081" rel="noopener noreferrer">Ask HN: Could you share your personal blog here?</a>, many commenters shared their own blog links.

There are several projects that converted the links to directories:
- <a href="https://news.ycombinator.com/item?id=36613727" rel="noopener noreferrer">Show HN: Blogs.hn – tiny blog directory</a> &rarr; <a href="https://blogs.hn" rel="noopener noreferrer">blogs.hn</a>
- <a href="https://news.ycombinator.com/item?id=36643086" rel="noopener noreferrer">Show HN: Hacker News Personal Blogs</a> &rarr; <a href="https://hn-blogs.kronis.dev" rel="noopener noreferrer">Hacker News Personal Blogs</a>
