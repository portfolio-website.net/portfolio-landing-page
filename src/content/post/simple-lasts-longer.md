---
publishDate: 2024-02-10T00:00:00Z
title: 'Simple Lasts Longer: Making a Map Stateful Using Scotch Tape and Web Storage API'
excerpt: Read about how simple technical solutions may benefit hobby projects.
image: ~/assets/images/pexels-fauxels-3183186.jpg
tags:
  - articles
---

<a href="https://www.pexels.com/photo/photo-of-people-having-meeting-3183186/" rel="noopener noreferrer" class="photo-credit">Photo by fauxels from Pexels</a>

<a href="https://newsletter.pnote.eu/p/simple-lasts-longer" rel="noopener noreferrer">Simple Lasts Longer: Making a Map Stateful Using Scotch Tape and Web Storage API</a> demonstrates how sometimes simple solutions reduce fragility, especially for hobby projects.  Also, see the <a href="https://news.ycombinator.com/item?id=38899922" rel="noopener noreferrer">Hacker News</a> comments.