---
publishDate: 2024-02-09T00:00:00Z
title: 'Soft Deletion Probably Isn’t Worth It (2022)'
excerpt: Review the logic behind why a soft delete vs. an actual delete may be preferable.
image: ~/assets/images/pexels-sora-shimazaki-5935787.jpg
tags:
  - articles
---

<a href="https://www.pexels.com/photo/crop-hacker-typing-on-laptop-with-information-on-screen-5935787/" rel="noopener noreferrer" class="photo-credit">Photo by Sora Shimazaki from Pexels</a>

<a href="https://brandur.org/soft-deletion" rel="noopener noreferrer">Soft Deletion Probably Isn’t Worth It</a> argues that there are cases where soft deletes may not make sense, such as with GDPR in Europe and when attempting to recover 'deleted' records.  Also, see the <a href="https://news.ycombinator.com/item?id=32156009" rel="noopener noreferrer">Hacker News</a> and <a href="https://www.reddit.com/r/programming/comments/w4bevc/soft_deletion_probably_isnt_worth_it/" rel="noopener noreferrer">Reddit</a> comments.