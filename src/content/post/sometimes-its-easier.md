---
publishDate: 2024-01-05T00:00:00Z
title: 'Sometimes It’s Easier to Mock without Moq (2020)'
excerpt: See some examples of how to mock for a C# unit test without requiring the Moq framework.
image: ~/assets/images/pexels-pixabay-248152.jpg
tags:
  - unit-tests
  - csharp
  - development
  - articles
---

<a href="https://www.pexels.com/photo/two-clear-glass-jars-beside-several-flasks-248152/" rel="noopener noreferrer" class="photo-credit">Photo by Pixabay from Pexels</a>

The post <a href="https://scotthannen.org/blog/2020/07/10/easier-to-mock-without-moq.html" rel="noopener noreferrer">Sometimes It’s Easier to Mock without Moq</a> describes alternatives to using Moq, including a simple mocked/faked class implementation and delegates.
