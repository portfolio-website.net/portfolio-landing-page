---
publishDate: 2024-01-07T00:00:00Z
title: 'TDD, Where Did It All Go Wrong? (2017)'
excerpt: Ian Cooper presents the benefits of test-driven development (TDD).
image: ~/assets/images/pexels-rfstudio-3825573.jpg
tags:
  - unit-tests
  - development
  - videos
---

<a href="https://www.pexels.com/photo/crop-laboratory-technician-examining-interaction-of-chemicals-in-practical-test-modern-lab-3825573/" rel="noopener noreferrer" class="photo-credit">Photo by RF._.studio from Pexels</a>

The DevTernity Conference includes an excellent presentation from Ian Cooper examining the advantages of test-driven development (TDD) in the context of real-world projects.

<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="//www.youtube-nocookie.com/embed/EZ05e7EMOLM?rel=0&amp;showinfo=0" width="300" height="150" allowfullscreen="allowfullscreen" data-mce-fragment="1"></iframe></div>