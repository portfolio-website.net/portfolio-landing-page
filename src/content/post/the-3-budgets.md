---
publishDate: 2024-02-01T00:00:00Z
title: 'The 3 Budgets'
excerpt: Discover the three sources of software development salaries.
image: ~/assets/images/pexels-lukas-574073.jpg
tags:
  - articles
---

<a href="https://www.pexels.com/photo/turned-on-laptop-computer-574073/" rel="noopener noreferrer" class="photo-credit">Photo by Lukas from Pexels</a>

<a href="https://swizec.com/blog/the-3-budgets/" rel="noopener noreferrer">The 3 Budgets</a> explains how the source of a software development salary impacts a career trajectory.  Also, see the <a href="https://news.ycombinator.com/item?id=38851051" rel="noopener noreferrer">Hacker News</a> comments.