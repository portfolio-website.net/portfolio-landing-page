---
publishDate: 2024-02-06T00:00:00Z
title: 'The Browsers Biggest TLS Mistake'
excerpt: Learn how misconfigured TLS servers may return different certificate responses that can result in inconsistent browser behavior.
image: ~/assets/images/pexels-kailani-mak-376704.jpg
tags:
  - articles
---

<a href="https://www.pexels.com/photo/macbook-pro-376704/" rel="noopener noreferrer" class="photo-credit">Photo by Kailani Mak from Pexels</a>

<a href="https://blog.benjojo.co.uk/post/browsers-biggest-tls-mistake" rel="noopener noreferrer">The Browsers Biggest TLS Mistake</a> details how browser certification validation is accomplished via a browser for an incomplete certificate chain when TLS servers are misconfigured.  Also, see the <a href="https://news.ycombinator.com/item?id=38902414" rel="noopener noreferrer">Hacker News</a> comments.