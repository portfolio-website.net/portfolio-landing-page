---
publishDate: 2024-01-27T00:00:00Z
title: 'The Hardest Part of Building Software Is Not Coding, It’s Requirements'
excerpt: See how unclear user requirements can make development so challenging when designing real-world solutions.
image: ~/assets/images/pexels-mikael-blomkvist-6476587.jpg
tags:
  - articles
---

<a href="https://www.pexels.com/photo/simple-workspace-at-home-6476587/" rel="noopener noreferrer" class="photo-credit">Photo by Mikael Blomkvist from Pexels</a>

<a href="https://stackoverflow.blog/2023/12/29/the-hardest-part-of-building-software-is-not-coding-its-requirements" rel="noopener noreferrer">The Hardest Part of Building Software Is Not Coding, It’s Requirements</a> explains how challenging software development can be since it's challenging to determine what users want.  Also, see the <a href="https://news.ycombinator.com/item?id=36597709" rel="noopener noreferrer">Hacker News</a> comments.