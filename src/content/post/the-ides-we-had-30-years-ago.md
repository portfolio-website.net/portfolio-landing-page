---
publishDate: 2024-01-02T00:00:00Z
title: 'The IDEs We Had 30 Years Ago… and We Lost'
excerpt: Dive into the code editors from the 80s and early 90s.
image: ~/assets/images/pexels-markus-spiske-4439901.jpg
tags:
  - development
  - articles
---

<a href="https://www.pexels.com/photo/a-laptop-screen-with-text-4439901/" rel="noopener noreferrer" class="photo-credit">Photo by Markus Spiske from Pexels</a>

<a href="https://blogsystem5.substack.com/p/the-ides-we-had-30-years-ago-and" rel="noopener noreferrer">The IDEs We Had 30 Years Ago… and We Lost</a> provides nostalgic details and screenshots from the old IDEs of the 1980s and early 1990s.  Check out additional comments via <a href="https://news.ycombinator.com/item?id=38792446" rel="noopener noreferrer">Hacker News</a>.
