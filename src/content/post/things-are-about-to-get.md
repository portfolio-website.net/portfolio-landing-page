---
publishDate: 2024-01-15T00:00:00Z
title: 'Things Are About to Get a Lot Worse for Generative AI'
excerpt: This post reviews experimental generative prompts that show how often infringement may occur.
image: ~/assets/images/pexels-tara-winstead-8386363.jpg
tags:
  - ai
  - articles
---

<a href="https://www.pexels.com/photo/robot-s-hand-on-blue-background-8386363/" rel="noopener noreferrer" class="photo-credit">Photo by Tara Winstead from Pexels</a>

<a href="https://garymarcus.substack.com/p/things-are-about-to-get-a-lot-worse" rel="noopener noreferrer">Things Are About to Get a Lot Worse for Generative AI</a> identifies experiment AI prompts that may lead to copyright infringement.  Also, see the <a href="https://news.ycombinator.com/item?id=38814093" rel="noopener noreferrer">Hacker News</a> comments.