---
publishDate: 2024-02-07T00:00:00Z
title: 'Web CGI Programs Aren’t Particularly Slow These Days'
excerpt: Review the potential benefits of CGI scripts running on modern servers.
image: ~/assets/images/pexels-mikhail-nilov-7583935.jpg
tags:
  - articles
---

<a href="https://www.pexels.com/photo/macbook-pro-on-white-table-7583935/" rel="noopener noreferrer" class="photo-credit">Photo by Mikhail Nilov from Pexels</a>

<a href="https://utcc.utoronto.ca/~cks/space/blog/web/CGINotSlow" rel="noopener noreferrer">Web CGI Programs Aren’t Particularly Slow These Days</a> revisits the performance and security of CGI scripts running on modern web servers.  Also, see the <a href="https://news.ycombinator.com/item?id=38912581" rel="noopener noreferrer">Hacker News</a> comments.