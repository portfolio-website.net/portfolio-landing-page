---
publishDate: 2024-01-28T00:00:00Z
title: 'What Is API Latency?'
excerpt: Learn about API latency and how it compares with API response time.
image: ~/assets/images/pexels-panumas-nikhomkhai-19353800.jpg
tags:
  - articles
---

<a href="https://www.pexels.com/photo/man-test-speed-network-in-data-center-room-19353800/" rel="noopener noreferrer" class="photo-credit">Photo by panumas nikhomkhai from Pexels</a>

<a href="https://blog.postman.com/what-is-api-latency/" rel="noopener noreferrer">What Is API Latency?</a> describes the details behind API latency, including how to measure it and help reduce its impact on a software product.