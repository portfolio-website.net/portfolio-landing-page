---
publishDate: 2024-01-17T00:00:00Z
title: 'Why I’m Skeptical of Low-Code'
excerpt: This article points out some of the "gotchas" when using low-code solutions.
image: ~/assets/images/pexels-sam-nam-1204148.jpg
tags:
  - development
  - articles
---

<a href="https://www.pexels.com/photo/macbook-pro-beside-clear-glass-jar-1204148/" rel="noopener noreferrer" class="photo-credit">Photo by Sam Nam from Pexels</a>

<a href="https://nick.scialli.me/blog/why-im-skeptical-of-low-code/" rel="noopener noreferrer">Why I’m Skeptical of Low-Code</a> raises some potential downsides that may occur when implementing low-code solutions.  Also, see the <a href="https://news.ycombinator.com/item?id=38816135" rel="noopener noreferrer">Hacker News</a> comments.