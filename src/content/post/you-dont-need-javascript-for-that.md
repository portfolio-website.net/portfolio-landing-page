---
publishDate: 2024-01-25T00:00:00Z
title: 'You Don’t Need JavaScript for That'
excerpt: See how to build some simple web page features without JavaScript.
image: ~/assets/images/pexels-designecologist-1779487.jpg
tags:
  - development
  - articles
---

<a href="https://www.pexels.com/photo/silver-imac-displaying-collage-photos-1779487/" rel="noopener noreferrer" class="photo-credit">Photo by Designecologist from Pexels</a>

<a href="https://www.htmhell.dev/adventcalendar/2023/2/" rel="noopener noreferrer">You Don’t Need JavaScript for That</a> demonstrates some web page features that can be implemented using HTML and CSS, without involving JavaScript at all.  Also, see the <a href="https://news.ycombinator.com/item?id=38497445" rel="noopener noreferrer">Hacker News</a> comments.