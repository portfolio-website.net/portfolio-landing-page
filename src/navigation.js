import { getPermalink, getBlogPermalink, getAsset } from './utils/permalinks';

export const headerData = {
  links: [    
    {
      text: 'Home',
      href: getPermalink('/#'),
    },
    /*{
      text: 'Services',
      href: getPermalink('/#services'),
    },*/
    {
      text: 'Research',
      href: getPermalink('/#research'),
    },
    {
      text: 'Development',
      href: getPermalink('/#development'),
    },
    /*{
      text: 'Projects',
      href: getPermalink('/#projects'),
    }*/
    {
      text: 'Contact',
      href: getPermalink('/#contact'),
    },
  ]
};

export const footerData = {
  links: [
    {
      title: 'Pages',
      links: [
        //{ text: 'Services', href: '/services/' },
        { text: 'Research', href: '/research/' },
        { text: 'Development', href: '/development/' },        
        //{ text: 'Projects', href: '/projects/' },
        { text: 'About', href: '/about/' },
      ],
    },
    {
      title: 'Features',
      links: [
        { text: 'Recent Activity', href: '/recent-activity/' },
        { text: 'Technologies', href: '/technologies/' },
      ],
    },
  ],
  socialLinks: [
  ],
  footNote: `
    <span class="w-5 h-5 md:w-6 md:h-6 md:-mt-0.5 bg-cover mr-1.5 rtl:mr-0 rtl:ml-1.5 float-left rtl:float-right rounded-sm bg-[url(~/assets/favicons/onwidget-favicon-32x32.png)]"></span>
    Made by <a class="text-blue-600 hover:underline dark:text-gray-200" href="https://onwidget.com/" rel="noopener noreferrer"> onWidget</a> · All rights reserved.
    <br />Template: <a class="text-blue-600 hover:underline dark:text-gray-200" href="https://github.com/onwidget/astrowind" rel="noopener noreferrer">AstroWind</a> 
    <br /><br />© 2024 portfolio-website.net
    <br />Home page image sources: 
      <a class="text-blue-600 hover:underline dark:text-gray-200" href="https://www.pexels.com/photo/macbook-pro-beside-black-ipad-on-white-table-5208871/" rel="noopener noreferrer">Karolina Grabowska</a>,
      <a class="text-blue-600 hover:underline dark:text-gray-200" href="https://www.pexels.com/photo/selective-focus-photography-of-wireless-keyboard-on-desk-317371/" rel="noopener noreferrer">Lukas</a>,
      <a class="text-blue-600 hover:underline dark:text-gray-200" href="https://www.pexels.com/photo/gray-laptop-computer-showing-html-codes-in-shallow-focus-photography-160107/" rel="noopener noreferrer">Negative Space</a>      
      
      <br />Icons made by <a class="text-blue-600 hover:underline dark:text-gray-200" href="https://www.flaticon.com/authors/freepik" rel="noopener noreferrer">Freepik</a> from <a class="text-blue-600 hover:underline dark:text-gray-200" href="https://www.flaticon.com/" rel="noopener noreferrer">www.flaticon.com</a>
  `,
};
