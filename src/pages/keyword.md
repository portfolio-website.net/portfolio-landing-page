---
title: 'Keyword'
layout: '~/layouts/MarkdownLayout.astro'
---

Please enter the keyword to continue.

<form action="/keyword" method="GET" class="border-box">
    <div id="keyword-error" style="display: none; color: #C70039;">Invalid keyword.  Please try again.</div> <label for="keyword" class="block text-sm font-medium">Keyword</label>
    <input type="text" id="keyword" name="keyword" required style="border: 1px inset lightgray; border-radius: 5px; background-color: white;" class="py-3 px-4 block w-full text-md rounded-lg border border-gray-200 dark:border-gray-700 bg-white dark:bg-slate-900" />
    <div class="mt-3">
        <button type="submit" style="border-radius: 5px" class="btn-primary">Submit</button>
    </div>
</form>

<hr />

If you don't have the keyword, simply [contact me](/contact).
